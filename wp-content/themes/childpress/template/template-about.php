<?php 
/**
 * Template Name: About Page
 * This is custm about page template file
 */
get_header(); 
Business_Responsiveness_breadcrumbs(); 
the_post(); 
?>
	<section class="rdn-about">
		<div class="container">
			<div class="row">
				
				<?php if( has_post_thumbnail() ): ?>
				<div class="col-md-6">
					<div class="about-image-area">
						<?php the_post_thumbnail(); ?>
					</div>
				</div>
				<?php endif; ?>
				<div class="col-md-<?php echo ( has_post_thumbnail()?'6':'12'); ?>">
					<?php 
						the_title('<h1>','</h1>'); 
					
						the_content();
					?>
				</div>
			</div>
		</div>
	</section><!-- .rdn-about -->
<?php get_footer(); ?>