=== ChildPress ===
Contributors:      88wpthemes
Tags:right-sidebar, threaded-comments, theme-options, sticky-post, custom-menu, featured-images, custom-logo, custom-header, custom-background, editor-style, blog
Requires PHP:      5.3
Requires at least: 4.1
Tested up to:      5.0.3
Stable tag:        1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

ChildPress is a versatile theme designed for child care services, schools, and other educational groups. This theme includes single color scheme, logo, social links, contact info and service etc. ChildPress is also responsive theme that compatible for all devices likes smartphone, tablet, laptop, or desktop computer. With its flexibility and included plugins, ChildPress is a great child care theme that is supported by a strong customer support team.

== Description ==

ChildPress is a versatile theme designed for child care services, schools, and other educational groups. This theme includes single color scheme, logo, social links, contact info and service etc.

**Languages Supported:**

* English
* Dansk
* Deutsch
* Ελληνικά
* Español
* Español de México
* Suomi
* Français
* हिन्दी
* Bahasa Indonesia
* Italiano
* 日本語
* 한국어
* मराठी
* Bahasa Melayu
* Norsk bokmål
* Nederlands
* Polski
* Português do Brasil
* Português
* Русский
* Svenska
* ไทย
* Tagalog
* Türkçe
* Українська
* Tiếng Việt
* 简体中文
* 香港中文版
* 繁體中文

**Contributing:**


== Installation ==

1. In your admin panel, nagivate to **Appearance > Themes** and click the **Add New** button.
2. Type **ChildPress** in the search form and press the **Enter** key on your keyboard.
3. Click the **Activate** button to begin using ChildPress on your website.
4. In your admin panel, navigate to **Appearance > Customize**.
5. Put the finishing touches on your website by adding a logo, and title.

== Copyright ==

ChildPress WordPress Theme is child theme of Business Responsiveness WordPress Theme, Copyright 2018 88wpthemes
ChildPress WordPress Theme is distributed under the terms of the GNU GPL.

ChildPress WordPress Theme, Copyright 2018 88wpthemes
ChildPress is distributed under the terms of the GNU GPL.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

ChildPress bundles the following third-party resources:

== Screenshots ==
* All images are licensed under [CC0]
	
= Screenshot Image =
Source: https://pixabay.com/en/hands-friendship-friends-children-2847508/

== Frequently Asked Questions ==
Please use WordPress support forum to ask any query.
	
== Upgrade Notice ==

= 1.1 =
* Style.css file updated.

= 1.0 =

* Resolved all issues.
	
== Changelog ==

= 0.1 =
* Initial upload.

For any help you can mail us at info[at]bangalorethemes.com