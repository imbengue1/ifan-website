<?php 
/**
 * This template for displaying contact sidebar
 *
 */
?>

<?php if( is_active_sidebar('sidebar-contact') ): ?>
<div class="col-md-4 secondary">
	<?php dynamic_sidebar('sidebar-contact'); ?>
</div>
<?php endif; ?>