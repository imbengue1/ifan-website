<?php 
/*
 * Overriding parent theme class of default data
 * 
 * Override theme default data of prent theme
 * 
 * @Since 0.4
 */
if( !class_exists('Business_Responsiveness_settings_array') ){
	
	class Business_Responsiveness_settings_array {

		function default_data(){
			return array(
			
			/* GENRAL SETTINGS */			
			'header-facebook-url'=>'',
			'header-twitter-url'=>'',
			'header-linkedin-url'=>'',
			'header-googleplus-url'=>'',
			'header-social-target'=>true,
			'top-header-hide'=>false,
			'layout' => false,
			'custom_color_scheme'=>'#C10036',
			
			'footer_copyright'=> '',
			'footer_socialicon_enable'=> true,
			'footer_socialicon_title'=> '',
			'footer_menu'=> true,
			'theme_color'=>'#C10036',
			'custom_color_enable'=>false,
			'footer_background'=>'#2c2c2c',
			'footer_info_background'=>'#242424',
			'site_title'=>'#ffffff',
			
			/* HOME PAGE SETTINGS */
			'slider_enable'=> true,
			'slider_animation_type'=> 'slide',
			'slider_speed'=> 3000,
			'slider_cat'=> 1,
			
			/* HOME PAGE SERVICE SETTINGS */
			'service_section_backgorund_color'=> '#ffffff',
			'service_section_image'=> '',
			'service_section_enable'=>true,
			'service_section_title'=>'',
			'service_section_description'=>'',
			'service_icon_one' => '',
			'service_content_one' => 0,
			'service_icon_two' => '',
			'service_content_two' => 0,
			'service_icon_three' => '',
			'service_content_three' => 0,

			
			/* HOME PAGE SHOP SETTINGS */
			'shop_section_backgorund_color'=> '#ffffff',
			'shop_section_image'=> '',
			'shop_section_image_repeat'=>'',
			'shop_section_enable'=>true,
			'shop_section_title'=>'',
			'shop_section_description'=>'',
			'shop_no_of_show'=>4,
			
			/* HOME PAGE NEWS SETTINGS */
			'news_section_backgorund_color'=> '#f3f3f3;',
			'news_section_image'=> '',
			'news_section_image_repeat'=>'',
			'news_section_enable'=>true,
			'news_section_title'=>'',
			'news_section_description'=>'',
			'news_no_of_show'=>4,
			'news_category_show'=>1,

			
			/* HOME PAGE CONTACT SETTINGS */
			'contact_section_enable'=> true,
			'contact_section_title'=>'',
			'contact_section_description'=>'',
			'contact_contactform_shortcode'=>'',
			
			/* BLOGS SETTINGS */
			'blog_feature_image_enable'=>true,
			'blog_meta_enable'=>true,
			
			/* PAGE SETTINGS */
			'page_feature_image_enable'=>true,
			'page_meta_enable'=>false,
			
			/* TYPOGRAPHY SETTINGS */
			
			'general_fontsize'=>16,
			'general_fontfamily'=>'Roboto',
			'general_fontstyle'=>'normal',
			
			'h1_fontsize'=>36,
			'h1_fontfamily'=>'Roboto Slab',
			'h1_fontstyle'=>'normal',
			
			'h2_fontsize'=>30,
			'h2_fontfamily'=>'Roboto Slab',
			'h2_fontstyle'=>'normal',
			
			'h3_fontsize'=>24,
			'h3_fontfamily'=>'Roboto Slab',
			'h3_fontstyle'=>'normal',
			
			'h4_fontsize'=>18,
			'h4_fontfamily'=>'Roboto Slab',
			'h4_fontstyle'=>'normal',
			
			'h5_fontsize'=>14,
			'h5_fontfamily'=>'Roboto Slab',
			'h5_fontstyle'=>'normal',
			
			'h6_fontsize'=>12,
			'h6_fontfamily'=>'Roboto Slab',
			'h6_fontstyle'=>'normal',
			
			);
		}
	}	
	
}
function childpress_script() {	
	$parent_style = 'parent-style';		
	wp_enqueue_style( 'childpress-style', get_stylesheet_uri(), array( $parent_style ) );	
}
add_action('wp_enqueue_scripts','childpress_script');

/*
 * Overriding parent theme functions
 * 
 * Override add_action for style css of the theme with Business_Responsiveness_color_scheme()
 * 
 * @Since 0.4
 */
add_action('wp_head','Business_Responsiveness_color_scheme');
function Business_Responsiveness_color_scheme(){
	$obj = new Business_Responsiveness_settings_array();
	$option = wp_parse_args(  get_option( 'business_r_option', array() ), $obj->default_data() );
	$color = $option['custom_color_scheme'];
	echo '<style>';
		Business_Responsiveness_set_color( $color );
	echo '</style>';
}
function Business_Responsiveness_set_color( $color ){
	$obj = new Business_Responsiveness_settings_array();
	$option = wp_parse_args(  get_option( 'business_r_option', array() ), $obj->default_data() );
	
	list($r, $g, $b) = sscanf( $color, "#%02x%02x%02x");
	?>
	a,
	a:hover,
	a:focus,
	a:active,
	.navbar-default .navbar-nav > li > a:hover,
	.navbar-default .navbar-nav > li > a:focus,
	.navbar-default .navbar-nav > .active > a:hover,
	.navbar-default .navbar-nav > .active > a:focus,
	.navbar-default .navbar-nav > .open > a,
	.navbar-default .navbar-nav > .open > a:hover,
	.navbar-default .navbar-nav > .open > a:focus,
	.navbar-default .navbar-nav > .dropdown.active > a,	
	#rdn-footer .widget .tagcloud a,
	.rdn-callout-btn:hover,
	.rdn-service-icon i.fa,
	.more-link,
	.rdn-portfolio-tabs li a,
	.team-title h5,
	.team-title:hover h5,
	.team-title:focus h5,
	.entry-title a:hover,
	.entry-title a:focus, 
	.entry-meta span a:hover, 
	.entry-meta span a:focus,
	button,
	button[disabled]:hover,
	button[disabled]:focus,
	input[type="button"],
	input[type="button"][disabled]:hover,
	input[type="button"][disabled]:focus,
	input[type="reset"],
	input[type="reset"][disabled]:hover,
	input[type="reset"][disabled]:focus,
	input[type="submit"],
	input[type="submit"][disabled]:hover,
	input[type="submit"][disabled]:focus,
	#rdn-footer .widget li a:hover, 
	#rdn-footer .widget li a:focus, 
	#rdn-footer .widget li a:active, 
	.widget .news-title a:hover, 
	.widget .news-title a:focus,
	.widget-title a, 
	.widget-title a:hover,  
	.widget-title a:focus, 
	.widget  li  a:hover, 
	.widget  li  a:focus,  
	.widget li:before, 
	.widget_calendar #wp-calendar th, 
	.tagcloud a, 
	.widget_text a:hover, 
	.widget_text a:focus, 
	#rdn-footer .widget .news-title a:hover, 
	#rdn-footer .widget .news-title a:focus,
	.rdn-footer-menu li a:hover, 
	.rdn-footer-menu li a:focus,
	#rdn-footer .widget a:hover, 
	#rdn-footer .widget a:focus,
	.rdn-copyright p > a, 
	.rdn-copyright p > a:hover, 
	.rdn-copyright p > a:focus,
	.rdn-sub-header li .active, 
	.rdn-sub-header ul li:before,
	.comments-title:after, 
	.comment-reply-title:after,
	.reply:before,
	.pagination li a,
	.page-links a,
	.entry-style-date span strong,
	.error404_title{
		color: <?php echo esc_attr($color); ?>;
	}
	
	::selection,
	#rdn-top-header,
	.dropdown-menu > .active > a, 
	.dropdown-menu > li > a:hover, 
	.dropdown-menu > li > a:focus,
	.dropdown-menu > .active > a:hover, 
	.dropdown-menu > .active > a:focus,
	.navbar-default .navbar-nav > .active > a,
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, 
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:focus,
	.navbar-default .navbar-nav .open .dropdown-menu > .active > a,
	.navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover, 
	.navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus,
	.carousel-indicators .active,
	.section-desc:before,
	.more-link:hover, .more-link:focus,
	.rdn-portfolio-tabs .active,
	.carousel-control-testi,
	.carousel-control-client.left,  
	.carousel-control-client.right,
	button:hover,
	button:focus,
	input[type="button"]:hover,
	input[type="button"]:focus,
	input[type="reset"]:hover,
	input[type="reset"]:focus,
	input[type="submit"]:hover,
	input[type="submit"]:focus,
    .contact-form-area .wpcf7-submit,
	.widget .widget-title:after, 
	.widget_search .search-submit,  
	.widget_calendar #wp-calendar caption, 
	.widget_calendar tbody a, 
	.widget_calendar tbody a:hover, 
	.widget_calendar tbody a:focus,
	.tagcloud a:hover,
	.tagcloud a:focus,
	.pagination .current a, 
	.pagination .current:hover a, 
	.pagination .current:focus a,
	.pagination li:hover a, 
	.pagination li:focus a,
	.nav-links li span.current,
	.nav-links li:hover span.current, 
	.nav-links li:focus span.current,
	.woocommerce #respond input#submit.alt, 
	 .woocommerce a.button.alt, 
	 .woocommerce button.button.alt, 
	 .woocommerce input.button.alt,
	 .woocommerce #respond input#submit.alt:hover, 
	 .woocommerce a.button.alt:hover, 
	 .woocommerce button.button.alt:hover, 
	 .woocommerce input.button.alt:hover,
	 .woocommerce #respond input#submit.disabled:hover, 
	 .woocommerce #respond input#submit:disabled:hover, 
	 .woocommerce #respond input#submit:disabled[disabled]:hover, 
	 .woocommerce a.button.disabled:hover, 
	 .woocommerce a.button:disabled:hover, 
	 .woocommerce a.button:disabled[disabled]:hover, 
	 .woocommerce button.button.disabled:hover, 
	 .woocommerce button.button:disabled:hover, 
	 .woocommerce button.button:disabled[disabled]:hover, 
	 .woocommerce input.button.disabled:hover, 
	 .woocommerce input.button:disabled:hover, 
	 .woocommerce input.button:disabled[disabled]:hover,
	 .woocommerce #respond input#submit.disabled, 
	 .woocommerce #respond input#submit:disabled, 
	 .woocommerce #respond input#submit:disabled[disabled], 
	 .woocommerce a.button.disabled, .woocommerce a.button:disabled, 
	 .woocommerce a.button:disabled[disabled], 
	 .woocommerce button.button.disabled, 
	 .woocommerce button.button:disabled, 
	 .woocommerce button.button:disabled[disabled], 
	 .woocommerce input.button.disabled, 
	 .woocommerce input.button:disabled, 
	 .woocommerce input.button:disabled[disabled],
	 .woocommerce #respond input#submit, 
	 .woocommerce a.button, 
	 .woocommerce button.button, 
	 .woocommerce input.button,
	 #add_payment_method .wc-proceed-to-checkout a.checkout-button, 
	 .woocommerce-cart .wc-proceed-to-checkout a.checkout-button, 
	 .woocommerce-checkout .wc-proceed-to-checkout a.checkout-button,
	 .woocommerce span.onsale,
	 .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
	 .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
	 .product_item .added_to_cart.wc-forward,
	 .post-style-header,
	.page-links > a:hover,
	.rdn-featured-image-overlay-inner a,
	.rdn_page_scroll,
	.rdn_page_scroll
	.pricing_wrapper.active .pricing_footer a,
	.footer-social-icons li:hover,
	.pricing_wrapper .pricing_footer a:hover, 
	.pricing_wrapper .pricing_footer a:focus,
	.pricing_ribben,
	.more-link:hover,
	.more-link:focus,
	.pricing_wrapper .pricing_footer a:hover, 
	.pricing_wrapper .pricing_footer a:focus,
	.error404_btn:hover,
	.error404_btn:focus{
		background-color: <?php echo esc_attr($color); ?>;
	}
	
 
	.dropdown-menu > li > a:hover, 
	.dropdown-menu > li > a:focus,
	.dropdown-menu > .active > a:hover, 
	.dropdown-menu > .active > a:focus,
	#rdn-footer .widget .tagcloud a
	{
    	border-bottom: 2px solid <?php echo esc_attr($color); ?>;
	}


	.carousel-caption h1{ 
		background-color: rgba(<?php echo esc_attr($r); ?>, <?php echo esc_attr($g); ?>, <?php echo esc_attr($b); ?>, 0.7); 
	}
	.carousel-caption h1{
		background-color: transparent;
	}

	.carousel-caption .rdn-slider-btn,
	.rdn-service-btn,
	.rdn-service-btn:hover,
	.team-more-link{ 
		background: linear-gradient(-48deg, <?php echo esc_attr($color); ?> 46%, rgba(0, 0, 0, 0.54) 48%);
	}
	.rdn-page-social li:hover{ 
		background: linear-gradient(-98deg, <?php echo esc_attr($color); ?> 46%, rgba(0, 0, 0, 0.54) 48%);
	}
	.rdn-page-social li:hover,
	.pagination li a, 
	.pagination li a:hover, 
	.pagination li a:focus,
	.nav-links li span.current, 
	.nav-links li:hover span.current, 
	.nav-links li:focus span.current,
	.page-links a,
	.more-link,
	button,
	button[disabled]:hover,
	button[disabled]:focus,
	input[type="button"],
	input[type="button"][disabled]:hover,
	input[type="button"][disabled]:focus,
	input[type="reset"],
	input[type="reset"][disabled]:hover,
	input[type="reset"][disabled]:focus,
	input[type="submit"],
	input[type="submit"][disabled]:hover,
	input[type="submit"][disabled]:focus,
	.error404_btn{ border:1px solid <?php echo esc_attr($color); ?>;}
	
	blockquote { 
		border-left: 4px solid <?php echo esc_attr($color); ?>;
	}
	
	.entry-style-date span strong{
		border:5px solid <?php echo esc_attr($color); ?>;
	}
	
	<?php if( $option['site_title'] != '' ){ ?>
	.site-title{ color: <?php echo esc_attr($option['site_title']); ?>; }
	<?php } ?>
	
	<?php if( $option['footer_background'] != '' ){ ?>
	.rdn-footer-top{
		    background: <?php echo esc_attr($option['footer_background']); ?>;
	}
	<?php } ?>
	
	<?php if( $option['footer_info_background'] != '' ){ ?>
	.rdn-footer-bottom{
		    background: <?php echo esc_attr($option['footer_info_background']); ?>;
	}
	<?php } ?>
	
	body { 
		<?php if( $option['general_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['general_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['general_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['general_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['general_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['general_fontstyle']); ?>; 
		<?php } ?>
	}
	h1, .h1 { 
		<?php if( $option['h1_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['h1_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['h1_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['h1_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['h1_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['h1_fontstyle']); ?>; 
		<?php } ?> 
	}
	h2, .h2 { 
		<?php if( $option['h2_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['h2_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['h2_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['h2_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['h2_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['h2_fontstyle']); ?>; 
		<?php } ?>
	}
	h3, .h3 { 
		<?php if( $option['h3_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['h3_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['h3_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['h3_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['h3_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['h3_fontstyle']); ?>; 
		<?php } ?> 
	}
	h4, .h4 { 
		<?php if( $option['h4_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['h4_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['h4_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['h4_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['h4_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['h4_fontstyle']); ?>; 
		<?php } ?>
	}
	h5, .h5 { 
		<?php if( $option['h5_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['h5_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['h5_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['h5_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['h5_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['h5_fontstyle']); ?>; 
		<?php } ?>
	}
	h6, .h6 { 
		<?php if( $option['h6_fontsize'] != '' ){ ?>
		font-size: <?php echo esc_attr($option['h6_fontsize']); ?>px; 
		<?php } ?>
		<?php if( $option['h6_fontfamily'] != '' ){ ?>
		font-family: '<?php echo esc_attr($option['h6_fontfamily']); ?>', sans-serif; 
		<?php } ?>
		<?php if( $option['h6_fontstyle'] != '' ){ ?>
		font-style: <?php echo esc_attr($option['h6_fontstyle']); ?>; 
		<?php } ?>
	}
	<?php
}

/*
 * Overriding parent theme filter function
 * 
 * Added filter for change header image with business_reponsiveness_custom_header_image()
 * 
 * @Since 0.4
 */
add_filter('business_reponsiveness_custom_header_image','childpress_header_image');
function childpress_header_image( $image ){
	$image = get_stylesheet_directory_uri() . '/images/seprator_back.jpg';
	return $image;
}

/*
 * Register Contact Widget
 * 
 * Contact widget area register for contact page
 * 
 * @Since 1.0
 */
add_action('widgets_init','childpress_sidebars',10);
 function childpress_sidebars(){	
	register_sidebar( array(
	'name' => __( 'Contact Sidebar', 'childpress' ),
	'id' => 'sidebar-contact',
	'description' => __( 'Contact page widget area', 'childpress' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h3 class="widget-title">',
	'after_title'   => '</h3>',
	) );
 }

/*
 * Exclude slider category posts from blog pages
 * 
 * 
 * @Since 1.0
 */
function childpress_exclude_single_posts_home( $query ) {

	if(is_admin()) return $query;

	if(is_page_template('template-homepage.php')) return $query;

	$option = get_option( 'business_r_option' );
	$slider_cat = '';
	if(isset($option['slider_cat'])){
		$slider_cat = absint( $option['slider_cat'] );
	}	
	if(strlen($slider_cat)!=0){
		$exclude_arg = array(); 
		$exclude_arg[] = $slider_cat;
		$query->set('category__not_in', $exclude_arg);
	}
}
add_action('pre_get_posts', 'childpress_exclude_single_posts_home');